package com.wearechurch.tool.service.controller;

import org.springframework.web.bind.annotation.PostMapping;

import com.wearechurch.tool.dto.Response;
import com.wearechurch.tool.service.constant.Path;
import com.wearechurch.tool.service.dto.Version;

public interface IVersionController {

	@PostMapping(Path.VERSION_VALIDATE)
	Response<Void> validateVersion(final Version version);
}
